/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall-popup-layer.h"

#include <glib/gi18n.h>
#include <locale.h>
#include <canterbury/gdbus/canterbury.h>

#define POPUP_LAYER_DEBUG_PRINT(...) // g_print( __VA_ARGS__)
static BarkwayLayer *pPopuplayer_proxy = NULL;
ClutterActor *pLayer_stage = NULL;
ClutterActor *pDefaultLayer;
ThornburyModel *model = NULL;
ThornburyModel *pSelctionPopupModel = NULL;
ThornburyItemFactory *itemFactory = NULL;
ThornburyItemFactory *pSlectionPopupItemFactory = NULL;
GObject *popup_object = NULL;
GObject *pSelectionPopupOject = NULL;
gchar *app_name=NULL;
gboolean toCancelPopupInfo=FALSE;
gboolean toCancelSelectionPopup=FALSE;
gboolean popup_info_shown=FALSE;
gboolean selection_popup_shown=FALSE;
G_DEFINE_TYPE (PopupLayer, popup_layer, CLUTTER_TYPE_ACTOR)

#define LAYER_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), POPUP_TYPE_LAYER, PopupLayerPrivate))

#define POPUP_LAYER_APP_NAME "org.apertis.Mildenhall.PopupLayer"
struct _PopupLayerPrivate
{
	CanterburyAppManager *app_mgr_proxy;
	CanterburyMutterPlugin *mutter_proxy;
    gint gesture_start_pos;
	ClutterActor *Dummy;
};
typedef enum _enModelInfo enModelInfo;
enum _enModelInfo {
	MSGSTYLE,
	MSGTEXT,
	BUTTONNUM,
	BUTTONTEXT,
	BUTTONIMAGE,
	IMAGEICON,
	IMAGEPATH,
	TIMER,
	COLUMN_LAST

};


typedef enum _enSelectionPopupModelInfo enSelectionPopupModelInfo;
enum _enSelectionPopupModelInfo {
	TEXT_ENTRY_MSGSTYLE,
	TEXT_ENTRY_MSGTEXT,
	IMAGEICON_KEY,
	IMAGEPATH_VALUE,
	SELECTION_POPUP_ROWS_INFO,
	SELECTION_POPUP_COLUMN_LAST

};

static void popup_layer_idle_animation_proxy_clb( GObject *source_object,
		GAsyncResult *res,
		gpointer pUserData);

gboolean shown_clb(PopupInfo *actor, gpointer data);
gboolean hidden_clb(PopupInfo *actor, gpointer data);
gboolean action_clb(PopupInfo *actor, gchar *button, gpointer value,
		gpointer data);



//gboolean init_layer_ui();

/*********************************************************************************************
 * Function:    v_popup_layer_dispose
 * Description: Call a finalize on the object
 * Parameters:  The object's reference
 * Return:      void
 ********************************************************************************************/
static void v_popup_layer_dispose(GObject *pObject) {
	POPUP_LAYER_DEBUG_PRINT("%s %d \n", __FUNCTION__, __LINE__);
	G_OBJECT_CLASS(popup_layer_parent_class)->dispose(pObject);
}

/*********************************************************************************************
 * Function:    v_popup_layer_finalize
 * Description: Dispose the object
 * Parameters:  The object's reference
 * Return:      void
 ********************************************************************************************/
static void v_popup_layer_finalize(GObject *pObject) {
	POPUP_LAYER_DEBUG_PRINT("%s %d \n", __FUNCTION__, __LINE__);
	G_OBJECT_CLASS(popup_layer_parent_class)->finalize(pObject);
}

/*********************************************************************************************
 * Function:    popup_layer_class_init
 * Description: Class initialisation function for the object type.
 *				Called automatically on the first call to g_object_new
 * Parameters:  The object's class reference
 * Return:      void
 ********************************************************************************************/
static void popup_layer_class_init(PopupLayerClass *pKlass) {
	POPUP_LAYER_DEBUG_PRINT("%s %d \n", __FUNCTION__, __LINE__);
	GObjectClass *pGobject_class = G_OBJECT_CLASS(pKlass);
	g_type_class_add_private(pGobject_class, sizeof(PopupLayerPrivate));
	pGobject_class->dispose = v_popup_layer_dispose;
	pGobject_class->finalize = v_popup_layer_finalize;
}

/*********************************************************************************************
 * Function:    p_popup_layer_new
 * Description: Creates a new popup-layer instance.
 * Parameters:
 * Return:      Newly created popup-layer actor
 ********************************************************************************************/
ClutterActor *p_popup_layer_new(void) {
	POPUP_LAYER_DEBUG_PRINT("%s %d \n", __FUNCTION__, __LINE__);
	return g_object_new(POPUP_TYPE_LAYER, NULL);
}

/*********************************************************************************************
 * Function:    popup_layer_init
 * Description: Instance initialisation function for the object type.
 *				Called automatically on every call to g_object_new
 * Parameters:  The object's reference
 * Return:      void
 ********************************************************************************************/
static void popup_layer_init(PopupLayer *pSelf) {
	POPUP_LAYER_DEBUG_PRINT("%s %d \n", __FUNCTION__, __LINE__);
	PopupLayerPrivate *pPriv;
	pSelf->priv = pPriv = LAYER_PRIVATE( pSelf );

	//init_layer_ui();
}

static void v_popup_reg_callback(GObject *pSource_object, GAsyncResult *pRes,
		gpointer pUser_data) {
	/*callback function for layer reg  */
	POPUP_LAYER_DEBUG_PRINT( "\n %s \n",__FUNCTION__ );
	barkway_layer_call_registration_status_finish(pPopuplayer_proxy, pRes,
			NULL);
}



static void v_popup_status_callback(GObject *pSource_object, GAsyncResult *pRes,
		gpointer pUser_data) {
	/*callback function for popup status * * */
	POPUP_LAYER_DEBUG_PRINT( "\n %s \n",__FUNCTION__ );
	barkway_layer_call_popup_status_finish(pPopuplayer_proxy, pRes, NULL);
}

static void v_popup_display_callback(GObject *pSource_object,
		GAsyncResult *pRes, gpointer pUser_data) {
	/*callback function for popup status */
	POPUP_LAYER_DEBUG_PRINT( "\n %s \n",__FUNCTION__ );
	barkway_layer_call_confirmation_result_finish(pPopuplayer_proxy, pRes,
			NULL);
}

static gboolean g_popup_display(BarkwayLayer *pObject,
		const gchar *pArg_app_name, gint64 inArg_popup_type,
		const gchar *pArg_title, GVariant *pArg_display_text,
		GVariant *pArg_popup_action, GVariant *pArg_image_data,
		gdouble dArg_timeout, const gchar *pArg_sound, gint voice_control_time,gpointer pUser_data) {


	GVariantIter iter;
	gchar *value;
	gchar *key;

  clutter_actor_show (pLayer_stage);

	POPUP_LAYER_DEBUG_PRINT( "\n %s \n",__FUNCTION__ );
	app_name=g_strdup(pArg_app_name);
	/* shown requried type of popup o popup-layer*/

	  gint row=thornbury_model_get_n_rows(model)-1;
		for(;row >= 0;row--)
		{
			thornbury_model_remove(model,row);
		}




	g_variant_iter_init(&iter, pArg_display_text);
	while (g_variant_iter_next(&iter, "{ss}", &key, &value)) {
		POPUP_LAYER_DEBUG_PRINT("key '%s' has value '%s'\n", key, value);
		thornbury_model_append(model, MSGSTYLE, key, MSGTEXT, value,TIMER,voice_control_time, -1);

	}

	row = 0;
	g_variant_iter_init(&iter, pArg_popup_action);
	while (g_variant_iter_next(&iter, "{ss}", &key, &value)) {
		POPUP_LAYER_DEBUG_PRINT("key '%s' has value '%s'\n", key, value);
		GValue val = { 0 };
		g_value_init(&val, G_TYPE_STRING);
		g_value_set_static_string(&val, key);
		thornbury_model_insert_value(model, row, BUTTONNUM, &val);
		g_value_set_static_string(&val, value);
		thornbury_model_insert_value(model, row, BUTTONTEXT, &val);
		row++;
	}
	row = 0;
	g_variant_iter_init(&iter, pArg_image_data);
	while (g_variant_iter_next(&iter, "{ss}", &key, &value)) {
		POPUP_LAYER_DEBUG_PRINT("key '%s' has value '%s'\n", key, value);
		GValue val = { 0 };
		g_value_init(&val, G_TYPE_STRING);
		g_value_set_static_string(&val, key);
		thornbury_model_insert_value(model, row, IMAGEICON, &val);
		g_value_set_static_string(&val, value);
		thornbury_model_insert_value(model, row, IMAGEPATH, &val);
		row++;
	}


	//g_object_get(itemFactory, "object", &popup_object, NULL);
	PopupInfo *actor = POPUP_INFO(popup_object);


	if(dArg_timeout<0)
	dArg_timeout=0;
	g_object_set(actor, "timeout", dArg_timeout, NULL);
	g_object_set(actor, "model", model, NULL);
	g_object_set(actor, "popupinfo-type", inArg_popup_type, NULL);
	//clutter_actor_add_child(pLayer_stage,CLUTTER_ACTOR(actor));

  popup_info_show (actor);


	return TRUE;
}


static void
on_popup_layer_app_mgr_name_vanished(GDBusConnection *connection,
                         const gchar     *name,
                         gpointer         user_data)
{
	PopupLayer *popuplayer=POPUP_LAYER(user_data);
    if(!popuplayer)
        return;

    if(NULL != popuplayer->priv->app_mgr_proxy)
        g_object_unref(popuplayer->priv->app_mgr_proxy);


}



static void
new_app_state_clb( CanterburyAppManager  *object,
                   const gchar             *app_name,
                   gint                     new_app_state,
                   GVariant                *arguments,
                   gpointer                 user_data)
{

    if( g_strcmp0(POPUP_LAYER_APP_NAME ,app_name ) == FALSE )
    {
        switch (new_app_state)
        {
        case CANTERBURY_APP_STATE_START:
            /* TODO INITIALIZE UI PART */

            break;

        case CANTERBURY_APP_STATE_BACKGROUND:
            /* see if it can do a clutter actor hide */
            break;

        case CANTERBURY_APP_STATE_SHOW:
            /* check if the application needs to resynchronize with its server */
            break;

        case CANTERBURY_APP_STATE_RESTART:
            /* check if the application needs to resynchronize with its server */
            break;

        case CANTERBURY_APP_STATE_OFF:
            /* store all persistence information immediately.. */


            break;

        case CANTERBURY_APP_STATE_PAUSE:
            /* store all persistence information immediately..*/
            break;

        default:

            break;
        }
    }
}


static void popup_layer_call_register_my_app_clb( GObject *source_object,
        GAsyncResult *res,
        gpointer user_data)
{
	PopupLayer *popuplayer=POPUP_LAYER(user_data);
    if(!popuplayer)
        return;
    GError *error=NULL;
    canterbury_app_manager_call_register_my_app_finish (
    		popuplayer->priv->app_mgr_proxy,
        res,
        &error);

}


static void idle_animation_stop_cb( GObject *source_object,GAsyncResult *res,gpointer pUserData)
{
	PopupLayer *popuplayer=POPUP_LAYER(pUserData);
		GError *error = NULL;
		canterbury_mutter_plugin_call_stop_idle_animation_finish(popuplayer->priv->mutter_proxy,res,&error);
}

static gboolean hide_global_popup( CanterburyAppManager *object,gpointer user_data )
{


	POPUP_LAYER_DEBUG_PRINT("\n  hide_global_popup \n");
	PopupLayer *popuplayer=POPUP_LAYER(user_data);
	canterbury_mutter_plugin_call_stop_idle_animation(popuplayer->priv->mutter_proxy,NULL,idle_animation_stop_cb,user_data);
	PopupInfo *actor = POPUP_INFO(popup_object);
	if(popup_info_shown == TRUE)
	{
		toCancelPopupInfo=TRUE;
        popup_info_hide(actor);
	}
	else if(selection_popup_shown == TRUE)
	{
		toCancelSelectionPopup=TRUE;
	 MildenhallSelectionPopup *pSelectionPopupActor = MILDENHALL_SELECTION_POPUP(pSelectionPopupOject);
			    v_mildenhall_selection_popup_hide( pSelectionPopupActor);
	}
  return FALSE;

}


static void popup_layer_appmgr_proxy_clb( GObject *source_object,
                                        GAsyncResult *res,
                                        gpointer user_data)
{

	PopupLayer *popuplayer=POPUP_LAYER(user_data);
    if(!popuplayer)
        return;
    GError *error=NULL;

    popuplayer->priv->app_mgr_proxy = canterbury_app_manager_proxy_new_finish(res , &error);
    if(popuplayer->priv->app_mgr_proxy == NULL)
    {
    	POPUP_LAYER_DEBUG_PRINT("error %s \n",g_dbus_error_get_remote_error(error));
    }
    g_signal_connect(popuplayer->priv->app_mgr_proxy,"new-app-state",(GCallback)new_app_state_clb,user_data);
    g_signal_connect(popuplayer->priv->app_mgr_proxy,"hide-global-popups",(GCallback)hide_global_popup,user_data);

    canterbury_app_manager_call_register_my_app (
    		popuplayer->priv->app_mgr_proxy,
        POPUP_LAYER_APP_NAME,
        0,
        NULL,
        popup_layer_call_register_my_app_clb,
        user_data);
}




static void
on_popup_layer_app_mgr_name_appeared (GDBusConnection *connection,
                          const gchar     *name,
                          const gchar     *name_owner,
                          gpointer         user_data)
{
    canterbury_app_manager_proxy_new(connection,
                                         G_DBUS_PROXY_FLAGS_NONE,
                                         "org.apertis.Canterbury",
                                         "/org/apertis/Canterbury/AppManager",
                                         NULL,
                                         popup_layer_appmgr_proxy_clb,
                                         user_data);


}

static gboolean g_popup_update_display_selection_popup(BarkwayLayer *pObject,
                                                       const gchar *pArg_app_name,
                                                       const gchar *pArg_title,
                                                       GVariant *pArg_rows_info,
                                                       gboolean arg_row_remove,
                                                       gpointer pUser_data)
{

	/*Building the hash table for rows_info in selection popup */

	GVariantIter inIncrementor;
	GVariant *pHashTable = NULL;

	MildenhallSelectionPopup *pSelectionPopup = MILDENHALL_SELECTION_POPUP(pSelectionPopupOject);
	g_object_set(pSelectionPopup, "remove", arg_row_remove, NULL);


	g_variant_iter_init ( &inIncrementor, pArg_rows_info );
	while ( (pHashTable = g_variant_iter_next_value (&inIncrementor)) != NULL)
	{
		thornbury_model_append(pSelctionPopupModel ,SELECTION_POPUP_ROWS_INFO , (gpointer)pHashTable , -1 );
	}
	return FALSE;
}


static gboolean g_popup_display_selection_popup(BarkwayLayer *pObject,
		const gchar *pArg_app_name, const gchar *pArg_title,
		GVariant *pArg_display_text, GVariant *pArg_rows_info,
		GVariant *pArg_image_data, gpointer pUser_data)
{
	POPUP_LAYER_DEBUG_PRINT( "\n %s \n",__FUNCTION__ );

	GVariantIter iter;
	gchar *pValue;
	gchar *pKey;

	POPUP_LAYER_DEBUG_PRINT( "\n %s \n",__FUNCTION__ );
	app_name=g_strdup(pArg_app_name);
	/* shown requried type of popup o popup-layer*/
	gint uinRow;
if(thornbury_model_get_n_rows(pSelctionPopupModel) > 0)
{

	uinRow=thornbury_model_get_n_rows(pSelctionPopupModel)-1;
	for(;uinRow >= 0;uinRow--)
	{

		thornbury_model_remove(pSelctionPopupModel,uinRow);
	}


}
	g_variant_iter_init(&iter, pArg_display_text);
	while (g_variant_iter_next(&iter, "{ss}", &pKey, &pValue))
	{
		POPUP_LAYER_DEBUG_PRINT("key '%s' has pValue '%s'\n", pKey, pValue);
		thornbury_model_append(pSelctionPopupModel, TEXT_ENTRY_MSGSTYLE, pKey, TEXT_ENTRY_MSGTEXT, pValue, -1);
	}


	/* Extracting the APP icon and MSG icon Image details and feed to model */
	uinRow = 0;
	g_variant_iter_init(&iter, pArg_image_data);
	while (g_variant_iter_next(&iter, "{ss}", &pKey, &pValue))
	{
		POPUP_LAYER_DEBUG_PRINT("pKey '%s' has pValue '%s'\n", pKey, pValue);
		GValue val = { 0 };
		g_value_init(&val, G_TYPE_STRING);
		g_value_set_static_string(&val, pKey);
		thornbury_model_insert_value(pSelctionPopupModel, uinRow, IMAGEICON_KEY, &val);
		g_value_set_static_string(&val, pValue);
		thornbury_model_insert_value(pSelctionPopupModel, uinRow, IMAGEPATH_VALUE, &val);
		uinRow++;
	}


	uinRow = 0;


	/*Building the hash table for rows_info in selection popup */

		GVariantIter inIncrementor;
	    GVariant *pHashTable = NULL;


		g_variant_iter_init ( &inIncrementor, pArg_rows_info );
	    while ( (pHashTable = g_variant_iter_next_value (&inIncrementor)) != NULL)
	    {
			GValue val = { 0 };
			g_value_init(&val, G_TYPE_POINTER);
			g_value_set_pointer(&val,pHashTable);
			thornbury_model_insert_value(pSelctionPopupModel, uinRow, SELECTION_POPUP_ROWS_INFO, &val);
	     // g_variant_unref (pHashTable);
	      uinRow++;
	    }

	MildenhallSelectionPopup *pSelectionPopup = MILDENHALL_SELECTION_POPUP(pSelectionPopupOject);


	g_object_set(pSelectionPopup,"remove" , FALSE , "popup-model", pSelctionPopupModel, NULL);

  clutter_actor_show (pLayer_stage);

	v_mildenhall_selection_popup_show(pSelectionPopup);
	return TRUE;
}

static gboolean g_popup_hide(BarkwayLayer *pObject,
		const gchar *pArg_app_name, gint64 inArg_popup_type,
		gpointer pUser_data) {

	POPUP_LAYER_DEBUG_PRINT("\n g_popup_hide  %d \n",popup_info_shown);
      if(popup_info_shown ==TRUE)
      {
	        PopupInfo *actor = POPUP_INFO(popup_object);

	    	     popup_info_hide(actor);
      }
      else if(selection_popup_shown == TRUE)
      {
    		MildenhallSelectionPopup *pSelectionPopupActor = MILDENHALL_SELECTION_POPUP(pSelectionPopupOject);
    		    	v_mildenhall_selection_popup_hide( pSelectionPopupActor);
      }
	return TRUE;
}

static void v_popup_proxy_clb(GObject *pSource_object, GAsyncResult *pRes,
		gpointer pUser_data) {
	POPUP_LAYER_DEBUG_PRINT("%s %d \n",__FUNCTION__,__LINE__);
	GError *pError;
	GCancellable *pCancellable = NULL;

	/* finishes the proxy creation and gets the proxy ptr */
	pPopuplayer_proxy = barkway_layer_proxy_new_finish(pRes, &pError);
	if (NULL == pPopuplayer_proxy) {
		g_warning("\n Error intializing the proxy");
		/*sending reg status to service to make sure that layer is initialized*/
		barkway_layer_call_registration_status(pPopuplayer_proxy, FALSE,
				pCancellable, v_popup_reg_callback, pUser_data);
	} else {
		/*sending reg status to service to make sure that layer is initialized*/
		barkway_layer_call_registration_status(pPopuplayer_proxy, TRUE,
				pCancellable, v_popup_reg_callback, pUser_data);
	}
	g_signal_connect(pPopuplayer_proxy, "display-popup",
			(GCallback) g_popup_display, pUser_data);
	g_signal_connect(pPopuplayer_proxy, "hide-popup", (GCallback) g_popup_hide,
			pUser_data);
	g_signal_connect(pPopuplayer_proxy, "display-selection-popup",
			(GCallback) g_popup_display_selection_popup, pUser_data);
    g_signal_connect(pPopuplayer_proxy, "update-display-selection-popup",
			(GCallback) g_popup_update_display_selection_popup, pUser_data);
}

static void v_popup_layer_name_appeared(GDBusConnection *pConnection,
		const gchar *pName, const gchar *pName_owner, gpointer pUser_data) {
	POPUP_LAYER_DEBUG_PRINT("%s \n", __FUNCTION__);

		/*Creates a new proxy for a remote interface exported by a connection on a message bus*/
		barkway_layer_proxy_new(pConnection,
				G_DBUS_PROXY_FLAGS_NONE, "org.apertis.Barkway", "/org/apertis/Barkway/Layer",
				NULL, v_popup_proxy_clb, pUser_data);
}

static void v_popup_layer_name_vanished(GDBusConnection *pConnection,
		const gchar *pName, gpointer pUser_data) {
	POPUP_LAYER_DEBUG_PRINT("%s \n", __FUNCTION__);
	if (NULL != pPopuplayer_proxy)
		g_object_unref(pPopuplayer_proxy);
}



gboolean selection_shown_clb(PopupInfo *actor, gpointer data) {

  POPUP_LAYER_DEBUG_PRINT("\n shown_clb \n");
  selection_popup_shown = TRUE;

  clutter_actor_show (pLayer_stage);

        //clutter_actor_set_size(pLayer_stage, 800, 480);
	barkway_layer_call_popup_status(pPopuplayer_proxy, app_name,
				"SHOWN", NULL, v_popup_status_callback, data);

	return FALSE;
}

gboolean selection_hidden_clb(PopupInfo *actor, gpointer data) {

  selection_popup_shown = FALSE;
  POPUP_LAYER_DEBUG_PRINT("\n hidden_clb \n");

  clutter_actor_hide (pLayer_stage);

        if(NULL ==app_name)
        app_name=g_strdup("dummyappname");
	/*after confirmation hidden status should be send*/
    if(TRUE == toCancelSelectionPopup)
    {

    	barkway_layer_call_popup_status(pPopuplayer_proxy, app_name,
    			"CANCELLED", NULL, v_popup_status_callback, data);
    	toCancelSelectionPopup=FALSE;
    }
    else
    {
    	barkway_layer_call_popup_status(pPopuplayer_proxy, app_name,
			"HIDDEN", NULL, v_popup_status_callback, data);
    }
	return FALSE;
}



static gboolean b_popup_layer_on_gesture_begin (ClutterGestureAction *action,ClutterActor *actor,gpointer user_data)
{

	PopupLayer *popuplayer=POPUP_LAYER(user_data);
    gfloat x,y;
    clutter_gesture_action_get_press_coords(action,0,&x,&y);
    popuplayer->priv->gesture_start_pos = y;
    return TRUE;
}

static gboolean b_popup_layer_on_gesture_progress (ClutterGestureAction *action,ClutterActor *actor,gpointer user_data)
{

	PopupLayer *popuplayer=POPUP_LAYER(user_data);

    gfloat x,y;
    clutter_gesture_action_get_motion_coords(action,0,&x,&y);

    if(((y - popuplayer->priv->gesture_start_pos) > 70.0))
    {
    	if(popup_info_shown ==TRUE)
    	      {
    		        PopupInfo *popup_info_actor = POPUP_INFO(popup_object);

    		        toCancelPopupInfo=TRUE;
    		    	 popup_info_hide(popup_info_actor);
    	      }
    	      else if(selection_popup_shown == TRUE)
    	      {
    	    		MildenhallSelectionPopup *pSelectionPopupActor = MILDENHALL_SELECTION_POPUP(pSelectionPopupOject);
    	    		         toCancelSelectionPopup=TRUE;
    	    		    	v_mildenhall_selection_popup_hide( pSelectionPopupActor);
    	      }
    }

    else
    {
        return TRUE;
    }


    return FALSE;
}



static void popup_layer_idle_animation_proxy_clb( GObject *source_object,
		GAsyncResult *res,
		gpointer pUserData)
{
	GError *error;
	PopupLayer *popuplayer=POPUP_LAYER(pUserData);
	popuplayer->priv->mutter_proxy = canterbury_mutter_plugin_proxy_new_finish(res , &error);
}

static void
on_popup_layer_idle_animation_name_appeared (GDBusConnection *connection,
                          const gchar     *name,
                          const gchar     *name_owner,
                          gpointer         user_data)
{
	 canterbury_mutter_plugin_proxy_new (connection,
	    			G_DBUS_PROXY_FLAGS_NONE,
	    			"org.apertis.Canterbury.Mutter.Plugin",
	    			"/org/apertis/Canterbury/Mutter/Plugin",
	    			NULL,
	    			popup_layer_idle_animation_proxy_clb,
	    			user_data);
}

static void
on_popup_layer_idle_animation_name_vanished (GDBusConnection *connection,
        const gchar     *name,
        gpointer         user_data)
{
	PopupLayer *popuplayer=POPUP_LAYER(user_data);
    if(!popuplayer)
        return;

    if(NULL != popuplayer->priv->mutter_proxy)
   	    g_object_unref(popuplayer->priv->mutter_proxy);
}

gint main(gint argc, gchar *argv[]) {
	GMainLoop *pMainLoop = NULL;

	/* Starts watching name on the bus specified by bus_type */
	/* calls name_appeared_handler and name_vanished_handler when the name is known to have a owner or known to lose its owner */

	int inErr = clutter_init(&argc, &argv);
	if (inErr != CLUTTER_INIT_SUCCESS) {
		return -1;
	}

	ClutterColor stage_color = { 0x00, 0x00, 0x00, 0x00 }; /* transparent color*/

	setlocale (LC_ALL, "");
	bindtextdomain (GETTEXT_PACKAGE, DATADIR "/locale");
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

	setlocale (LC_MESSAGES, "en_US");

	/* Get the stage and set its size and color: */
	pLayer_stage = clutter_stage_new();
        clutter_actor_set_size(pLayer_stage, 800,480);
        clutter_stage_set_use_alpha(CLUTTER_STAGE(pLayer_stage), TRUE);

  clutter_actor_set_background_color (CLUTTER_ACTOR (pLayer_stage),
                                      &stage_color);
  clutter_actor_set_reactive (CLUTTER_ACTOR (pLayer_stage), FALSE);

    pDefaultLayer = p_popup_layer_new();
clutter_actor_add_child(pLayer_stage,CLUTTER_ACTOR(pDefaultLayer));
clutter_actor_set_size(pDefaultLayer,800,480);
clutter_actor_set_reactive(pDefaultLayer,TRUE);
 ClutterAction *gesture_action = clutter_gesture_action_new ();
    clutter_actor_add_action (pDefaultLayer, gesture_action);
    g_signal_connect (gesture_action, "gesture-begin", G_CALLBACK (b_popup_layer_on_gesture_begin), pDefaultLayer);
    g_signal_connect (gesture_action, "gesture-progress", G_CALLBACK (b_popup_layer_on_gesture_progress), pDefaultLayer);

	itemFactory = thornbury_item_factory_generate_widget_with_props(POPUP_TYPE_INFO, PKGDATADIR"/mildenhall-popup-layer-prop.json");
        g_object_get(itemFactory, "object", &popup_object, NULL);
	PopupInfo *actor = POPUP_INFO(popup_object);
        g_signal_connect(actor, "popup-shown", (GCallback) shown_clb, NULL);
	g_signal_connect(actor, "popup-hidden", (GCallback) hidden_clb, NULL);
	g_signal_connect(actor, "popup-action", (GCallback) action_clb, NULL);

        clutter_actor_add_child(pDefaultLayer,CLUTTER_ACTOR(actor));
        clutter_actor_hide(CLUTTER_ACTOR(actor));
	model = THORNBURY_MODEL(thornbury_list_model_new(COLUMN_LAST, G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL, G_TYPE_STRING, NULL, G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL, G_TYPE_STRING, NULL, G_TYPE_STRING, NULL,
			G_TYPE_INT, NULL, -1));



	/* Selection Popup creation */


	//g_object_new(MILDENHALL_TYPE_SELECTION_POPUP)
	pSlectionPopupItemFactory = thornbury_item_factory_generate_widget_with_props(MILDENHALL_TYPE_SELECTION_POPUP, PKGDATADIR"/mildenhall-selection-popup-prop.json");
	g_object_get(pSlectionPopupItemFactory, "object", &pSelectionPopupOject, NULL);
	MildenhallSelectionPopup *pSelectionPopupActor = MILDENHALL_SELECTION_POPUP(pSelectionPopupOject);
	g_signal_connect(pSelectionPopupActor, "popup-shown", (GCallback) selection_shown_clb, NULL);
	g_signal_connect(pSelectionPopupActor, "popup-hidden", (GCallback) selection_hidden_clb, NULL);
	g_signal_connect(pSelectionPopupActor, "popup-action", (GCallback) action_clb, NULL);
	clutter_actor_add_child(pDefaultLayer,CLUTTER_ACTOR(pSelectionPopupActor));
	clutter_actor_hide(CLUTTER_ACTOR(pSelectionPopupActor));

	//TODO : G_TYPE_VARIANT for rows info
	pSelctionPopupModel = THORNBURY_MODEL(thornbury_list_model_new(SELECTION_POPUP_COLUMN_LAST, G_TYPE_STRING, NULL,
	                                           G_TYPE_STRING, NULL, G_TYPE_STRING, NULL,G_TYPE_STRING, NULL, G_TYPE_POINTER, NULL,-1));



	g_bus_watch_name (G_BUS_TYPE_SESSION,
	                      "org.apertis.Canterbury",
	                      G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
	                      on_popup_layer_app_mgr_name_appeared,
	                      on_popup_layer_app_mgr_name_vanished,
	                      pDefaultLayer,
	                      NULL);

	g_bus_watch_name (G_BUS_TYPE_SESSION,
		              "org.apertis.Canterbury.Mutter.Plugin",
		              G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
		              on_popup_layer_idle_animation_name_appeared,
		              on_popup_layer_idle_animation_name_vanished,
		              pDefaultLayer,
		              NULL);



        g_bus_watch_name(G_BUS_TYPE_SESSION, "org.apertis.Barkway",
			G_BUS_NAME_WATCHER_FLAGS_AUTO_START, v_popup_layer_name_appeared,
			v_popup_layer_name_vanished, pDefaultLayer, NULL);

  clutter_actor_hide (pLayer_stage);

	pMainLoop = g_main_loop_new(NULL, FALSE);
	g_main_loop_run(pMainLoop);

	return 0;
}

gboolean shown_clb(PopupInfo *actor, gpointer data) {

	POPUP_LAYER_DEBUG_PRINT("\n shown_clb \n");
	popup_info_shown=TRUE;

  clutter_actor_show (pLayer_stage);
	barkway_layer_call_popup_status(pPopuplayer_proxy, app_name,
				"SHOWN", NULL, v_popup_status_callback, data);

	return FALSE;
}

gboolean hidden_clb(PopupInfo *actor, gpointer data) {

  popup_info_shown = FALSE;
  clutter_actor_hide (pLayer_stage);

        if(NULL ==app_name)
        app_name=g_strdup("dummyappname");
	/*after confirmation hidden status should be send*/
    if(TRUE == toCancelPopupInfo)
    {

    	barkway_layer_call_popup_status(pPopuplayer_proxy, app_name,
    			"CANCELLED", NULL, v_popup_status_callback, data);
    	toCancelPopupInfo=FALSE;
    }
    else
    {
    	barkway_layer_call_popup_status(pPopuplayer_proxy, app_name,
			"HIDDEN", NULL, v_popup_status_callback, data);
    }
	return FALSE;
}

gboolean action_clb(PopupInfo *actor, gchar *button, gpointer value,
		gpointer data) {

	POPUP_LAYER_DEBUG_PRINT("\n button %s\n", button);
	/*sending confirmation response through this method*/
		barkway_layer_call_confirmation_result(pPopuplayer_proxy,
				app_name, button,GPOINTER_TO_INT(value), NULL, v_popup_display_callback,
				data);
	POPUP_LAYER_DEBUG_PRINT("\n value %d \n", GPOINTER_TO_INT(value));
	return FALSE;
}
