/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __POPUP_LAYER_H__
#define __POPUP_LAYER_H__

#include <glib.h>
#include <glib/gstdio.h>
#include <glib-object.h>
#include <glib/gprintf.h>
#include <stdio.h>
#include <stdlib.h>
#include <gio/gio.h>
#include <barkway/barkway.h>
#include <barkway/org.apertis.Barkway.Layer.h>
#include <unistd.h>
#include <clutter/clutter.h>
#include <thornbury/thornbury.h>
#include <mildenhall/mildenhall.h>
#include <string.h>
#include <gdk/gdk.h>
#include <gdk/gdkx.h>

G_BEGIN_DECLS

#define POPUP_TYPE_LAYER popup_layer_get_type()

#define POPUP_LAYER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  POPUP_TYPE_LAYER, PopupLayer))

#define POPUP_LAYER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  POPUP_TYPE_LAYER, PopupLayerClass))

#define POPUP_IS_LAYER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  POPUP_TYPE_LAYER))

#define POPUP_IS_LAYER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  POPUP_TYPE_LAYER))

#define POPUP_LAYER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  POPUP_TYPE_LAYER, PopupLayerClass))

typedef struct _PopupLayer PopupLayer;
typedef struct _PopupLayerClass PopupLayerClass;
typedef struct _PopupLayerPrivate PopupLayerPrivate;

struct _PopupLayer
{
  ClutterActor parent;
  PopupLayerPrivate *priv;
};

struct _PopupLayerClass
{
  ClutterActorClass parent_class;
};

GType popup_layer_get_type (void) G_GNUC_CONST;

ClutterActor *p_popup_layer_new (void);

G_END_DECLS

#endif /* __POPUP_LAYER_H__ */
